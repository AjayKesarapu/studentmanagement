import { CanActivateFn } from '@angular/router';
import { StuService } from './stu.service';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {
  //return false;
  let service = inject(StuService);
  return service.getLoginStatus();
};
