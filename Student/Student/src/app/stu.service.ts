import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StuService {
  isUserLogged: boolean;

  constructor(private http:HttpClient) { 
    this.isUserLogged = false;
  }

  //Login
  setUserLogIn() {
    this.isUserLogged = true;
  }
  //Logout
  setUserLogOut() {
    this.isUserLogged = false;
  }
  getLoginStatus(): boolean {
    return this.isUserLogged;
  }
  getAllStudents(): any {
    return this.http.get("http://localhost:8080/getAllStudents");
  }
  getStudentById(sId: any): any {
    return this.http.get('http://localhost:8080/getStudentById/' + sId);
  }
  getAllDepartments(): any {
    return this.http.get('http://localhost:8080/getAllDepartments');
  }
  registerStudent(stu: any): any {
    return this.http.post('registerStudent', stu);
  }
  deleteStudent(sId: any): any {
    return this.http.delete('deleteStuById/' + sId);
  }
  updateStudent(stu: any): any {
    return this.http.put('updateStudent', stu);
  }
  getAllBooks(): any {
    return this.http.get("http://localhost:8080/getAllBooks");
  }
}
