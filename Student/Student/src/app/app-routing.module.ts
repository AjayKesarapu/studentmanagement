import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistarionComponent } from './registarion/registarion.component';
import { ShowStudentsComponent } from './show-students/show-students.component';
import { LogoutComponent } from './logout/logout.component';
import { ShowStudentByIdComponent } from './show-student-by-id/show-student-by-id.component';
import { authGuard } from './auth.guard';
import { BooksComponent } from './books/books.component';
import { CartComponent } from './cart/cart.component';

const routes: Routes = [
  {path:"",            component:LoginComponent},
  {path:"Login",       component:LoginComponent},
  {path:"Register",    component:RegistarionComponent},
  {path:"ShowStudents",  canActivate:[authGuard],component: ShowStudentsComponent},
  {path:"ShowStudentById", canActivate:[authGuard],component:ShowStudentByIdComponent},
  {path:"Logout",       canActivate:[authGuard],component:LogoutComponent},
  {path:"books",     canActivate:[authGuard], component:BooksComponent},
  {path:"cart",     canActivate:[authGuard], component:CartComponent}
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
