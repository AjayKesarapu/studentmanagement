import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ShowStudentsComponent } from './show-students/show-students.component';
import { ShowStudentByIdComponent } from './show-student-by-id/show-student-by-id.component';
import { RegistarionComponent } from './registarion/registarion.component';
import { GenderPipe } from './gender.pipe';
import { HeaderComponent } from './header/header.component';
import { LogoutComponent } from './logout/logout.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { HttpClientModule } from '@angular/common/http';
import { CartComponent } from './cart/cart.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShowStudentsComponent,
    ShowStudentByIdComponent,
    RegistarionComponent,
    GenderPipe,
    HeaderComponent,
    LogoutComponent,
    BooksComponent,
    CartComponent,
  
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule, 
    HttpClientModule

  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
