import { Component,OnInit } from '@angular/core';
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';
import { StuService } from '../stu.service';

@Component({
  selector: 'app-show-student-by-id',
  templateUrl: './show-student-by-id.component.html',
  styleUrls: ['./show-student-by-id.component.css']
})
export class ShowStudentByIdComponent implements OnInit {

  stuId:any
  student:any
  // students:any
  emailId: any;
   constructor( private service:StuService){
    this.emailId = localStorage.getItem("emailId");
    // this.students=[
    //   {
    //   "stuId":101,
    //   "stuName":"Ajay",
    //   "dept":"cse",
    //   "gender":"Male", 
    //   "emailId":"ajay@gmail.com",
    //   "password":"Ajay"
    // },
    // {
    //   "stuId": 102,
    //   "stuName": "Jane ",
    //   "dept": "EEE",
    //   "gender": "Female",
    //   "emailId": "jane.smith@gmail.com",
    //   "password": "jane"
    // },
    // {
    //   "stuId": 103,
    //   "stuName": "sai",
    //   "dept": "CSE",
    //   "gender": "Male",
    //   "emailId": "sai@gmail.com",
    //   "password": "sai"
    // },
    // {
    //   "stuId": 104,
    //   "stuName": "Surendra",
    //   "dept": "Mech",
    //   "gender": "Male",
    //   "emailId": "surendra@gmail.com",
    //   "password": "surendra"
    // }, {
    //   "stuId": 105,
    //   "stuName": "harshith",
    //   "dept": "ECE",
    //   "gender": "Female",
    //   "emailId": "Harshith@gmail.com",
    //   "password": "goodha"
   // }];
  }
  ngOnInit() {
    
  }
  getStudent() {
    this.student = null;

    this.service.getStudentById(this.stuId).subscribe((data: any) => {
      this.student = data;
      console.log(data);
    });
     
    
  }
}
