import { Component } from '@angular/core';
import { StuService } from '../stu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent {
  constructor(private service: StuService, private router: Router) {
    this.service.setUserLogOut();
    this.router.navigate(['Login']);
  }

}
