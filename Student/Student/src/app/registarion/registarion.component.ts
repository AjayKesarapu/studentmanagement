import { Component, OnInit } from '@angular/core';
import { StuService } from '../stu.service';

@Component({
  selector: 'app-registarion',
  templateUrl: './registarion.component.html',
  styleUrls: ['./registarion.component.css']
})
export class RegistarionComponent implements OnInit {
  student:any;
  departments: any;

  constructor( private service:StuService){
    this.student={
      "stuId":"",
      "stuName":"",
      "gender":"",
      "emailId":"",
      "password":"",
      "dept": {
        "deptId":""
      }

    }
  }

  ngOnInit() {
    this.service.getAllDepartments().subscribe((data: any) => {
      this.departments = data;
      console.log(data);
    });
  }


    
  
  registerSubmit( registerForm:any) {
    //alert("Employee Registration Success");
    console.log(this.student);
  }
  register() {
    alert("Student Registration Success");
    console.log(this.student);
    this.service.registerStudent(this.student).subscribe((data: any) => {
      console.log(data);
    });
  }

}
