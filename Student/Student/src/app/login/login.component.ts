import { Component, OnInit } from '@angular/core';
import { StuService } from '../stu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

stu:any
emailId: any;
  password: any;
  students: any;
  
  
  constructor(private service: StuService, private router:Router) {
    this.students=[
      {
      "stuId":101,
      "stuName":"Ajay",
      "dept":"cse",
      "gender":"Male", 
      "emailId":"ajay@gmail.com",
      "password":"Ajay"
    },
    {
      "stuId": 102,
      "stuName": "Jane Smith",
      "dept": "EEE",
      "gender": "Female",
      "emailId": "jane.smith@gmail.com",
      "password": "jane"
    },
    {
      "stuId": 103,
      "stuName": "sai",
      "dept": "CSE",
      "gender": "Male",
      "emailId": "sai@gmail.com",
      "password": "sai"
    },
    {
      "stuId": 104,
      "stuName": "Surendra",
      "dept": "Mech",
      "gender": "Male",
      "emailId": "surendra@gmail.com",
      "password": "surendra"
    }, {
      "stuId": 105,
      "stuName": "harshith",
      "dept": "ECE",
      "gender": "Female",
      "emailId": "Harshith@gmail.com",
      "password": "goodha"
    }];
  }
  ngOnInit() {
   
  }
  loginSubmit(loginForm:any){
    localStorage.setItem("emailId", loginForm.emailId);

    if(loginForm.emailId == 'HR' && loginForm.password == 'HR') {
      this.service.setUserLogIn();
      this.router.navigate(['ShowStudents']);
    }
else{
    this.students.forEach((student: any) => {
      if(loginForm.emailId == student.emailId && loginForm.password == student.password) {
         this.stu=student
        
      }
   });
    if(this.stu!=null){
      
      this.service.setUserLogIn();
      this.router.navigate(['books']);
      
    } else{
      alert("Inavalid Credentials")
    }

  }
  }
  login() {
    
   this.students.forEach((student: any) => {
         if(this.emailId == student.emailId && this.password == student.password) {
            this.stu=student
         }
      });
      if(this.stu!=null){
        alert('Login Succses')
        this.service.setUserLogIn();
      this.router.navigate(['ShowStudents']);
      } else{
        alert("Inavalid Credentials")
      }
    
  }
}
