import { Component, OnInit } from '@angular/core';
import { StuService } from '../stu.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  
  books:any;
  constructor( private service: StuService){
  //   this.books = [

  // {id:1001, name:"Engineering Chemistry",   price:883.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10001.jpeg"},
  //     {id:1002, name:"Java Programming ", price:249.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10002.jpeg"},
  //     {id:1003, name:"Monk Who sold his ferrari",  price:399.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10003.jpeg"},
  //     {id:1004, name:"Harry Potter",  price:449.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10004.jpeg"},
  //     {id:1005, name:"Basics of c",    price:549.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10005.jpeg"},
  //     {id:1006, name:"Engineering Drawing",    price:699.00, description:"No Cost EMI Applicable", imgsrc:"assets/Images/10006.jpeg"}
  //   ];
  }


  ngOnInit() {
    this.service.getAllBooks().subscribe((data: any) => {
      this.books = data;
      console.log(data);
   });
   
  }
  addToCart(book: any) {
    console.log(book);
  }

}
