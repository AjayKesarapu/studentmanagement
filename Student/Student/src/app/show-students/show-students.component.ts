import { Component,OnInit } from '@angular/core';
import { StuService } from '../stu.service';


declare var jQuery: any;
@Component({
  selector: 'app-show-students',
  templateUrl: './show-students.component.html',
  styleUrls: ['./show-students.component.css']
})
export class ShowStudentsComponent implements OnInit {

students:any
  student: any;
  departments: any;
  constructor(private service: StuService){
    this.student={
      "stuId":"",
      "stuName":"",
      "gender":"",
      "emailId":"",
      "password":"",
      "dept": {
        "deptId":""
      }

    }
  }
  
  ngOnInit() {
    this.service.getAllStudents().subscribe((data: any) => {
      this.students = data;
      console.log(data);
    });
    this.service.getAllDepartments().subscribe((data: any) => {
      this.departments = data;
    });
  }

  editStudent(stu: any) {
    
  
    this.student=stu;
    jQuery('#stuModel').modal('show');
  }

  updateStudent() {
    this.service.updateStudent(this.student).subscribe((data: any) => { 
      console.log(data); 
    });
  }
  deleteStudent(stu: any) {

    //Delete the Employee Record form the Database
    this.service.deleteStudent(stu.sId).subscribe((data: any) => {
      console.log(data);
    });

    //Delete the Employee Record only in UI (HTML)
    const i = this.students.findIndex((student: any) => {
      return student.sId == stu.sId;
    });
    this.students.splice(i, 1);
  }
  
 



}
