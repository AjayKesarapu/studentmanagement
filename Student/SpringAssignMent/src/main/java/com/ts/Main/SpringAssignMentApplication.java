package com.ts.Main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@EnableJpaRepositories(basePackages="com.ts.DAO")
@EntityScan(basePackages="com.ts.Model")
@SpringBootApplication(scanBasePackages="com.ts")
//@SpringBootApplication
public class SpringAssignMentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAssignMentApplication.class, args);
	}

}
