package com.ts.Main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ts.DAO.BooksDAO;
import com.ts.Model.Books;

@RestController
public class BooksController {
	
	@Autowired
	BooksDAO bookdao;
	
	

	@GetMapping("getAllBooks")
	public List<Books> getAllBookss() {
		return bookdao.getAllBooks();
	}
	@GetMapping("getBookByName/{name}")
	public Books getBookByName(@PathVariable("name") String name){
		return bookdao.getBookByName(name);}
	
	@GetMapping("getBookByid/{id}")
	public Books showBook(@PathVariable("id") int Id){
		return bookdao.getBookbyid(Id);
	}
	
	
	@PostMapping("registerBook")
	public Books registerBook(@RequestBody Books book) {
		return bookdao.registerBook(book);
	}

}
