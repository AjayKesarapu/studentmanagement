package com.ts.Main;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ts.DAO.StudentDAO;
import com.ts.Model.BankAccount;
import com.ts.Model.Student;
@RestController
public class StudentController {
	@Autowired
	StudentDAO studentDAO;
	
	


	@RequestMapping("getAllStudents")
	public List<Student> getAllStudents(){
		return studentDAO.getAllStudents();
	}
	@RequestMapping("getStudentById/{Id}")
	public Student getStudentById(@PathVariable("Id")int id){
		return studentDAO.getStudentbyid(id);
		
	}
	@RequestMapping("getStudentByName/{name}")
	public Student getStudentByName(@PathVariable("name")String name){
		return studentDAO.getStudentByName(name);
	}
	@PostMapping("registerStudent")
	public Student registerStudent(@RequestBody Student stud) {
		return studentDAO.registerStudent(stud);
	}
	@PutMapping("updateStudent")
	public Student update(@RequestBody Student stu) {
		return studentDAO.updateStu(stu);
	}
	
	@DeleteMapping("deleteStuById/{id}")
	public String deleteStuById(@PathVariable("id") int sId) {
		studentDAO.deleteStuById(sId);
		return "Employee Record Deleted...";
	}
}
