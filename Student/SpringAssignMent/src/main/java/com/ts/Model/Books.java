package com.ts.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Books {
	@Id
	@GeneratedValue
	private int id;
	private String name;
	private double price;
	private String description;
	private String imgsrc;
	public Books() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Books(int id, String name, double price, String description, String imgsrc) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.description = description;
		this.imgsrc = imgsrc;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImgsrc() {
		return imgsrc;
	}
	public void setImgsrc(String imgsrc) {
		this.imgsrc = imgsrc;
	}
	@Override
	public String toString() {
		return "Books [id=" + id + ", name=" + name + ", price=" + price + ", description=" + description + ", imgsrc="
				+ imgsrc + "]";
	}
	

}
