package com.ts.Model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Department {
	@Id
	@GeneratedValue
private int  deptId;
private String deptName;
private String hOD;

@JsonIgnore
@OneToMany(mappedBy="department")
List<Student> stuList = new ArrayList<Student>();

public Department() {
	super();
	// TODO Auto-generated constructor stub
}
public Department(int deptId, String deptName, String hOD) {
	super();
	this.deptId = deptId;
	this.deptName = deptName;
	this.hOD = hOD;
}
public int getDeptId() {
	return deptId;
}
public void setDeptId(int deptId) {
	this.deptId = deptId;
}
public String getDeptName() {
	return deptName;
}
public void setDeptName(String deptName) {
	this.deptName = deptName;
}
public String getHOD() {
	return hOD;
}
public void setHOD(String hOD) {
	 this.hOD = hOD;
}
@Override
public String toString() {
	return "Department [deptId=" + deptId + ", deptName=" + deptName + ", hOD=" + hOD + ", stuList=" + stuList + "]";
}


}
