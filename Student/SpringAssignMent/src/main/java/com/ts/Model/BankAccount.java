package com.ts.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class BankAccount {
	@Id
	@GeneratedValue
private  int bId;
private String name;
private double balance;
public BankAccount() {
	super();
	// TODO Auto-generated constructor stub
}
public BankAccount(int bNumber, String name, double balance) {
	super();
	this.bId = bNumber;
	this.name = name;
	this.balance = balance;
}
public int getNumber() {
	return bId;
}
public void setNumber(int number) {
	this.bId = number;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public double getBalance() {
	return balance;
}
public void setBalance(double balance) {
	this.balance = balance;
}
@Override
public String toString() {
	return "BankAccount [number=" + bId + ", name=" + name + ", balance=" + balance + "]";
}

}
