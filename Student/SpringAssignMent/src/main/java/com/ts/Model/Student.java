package com.ts.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;




@Entity
public class Student {
	@Id
	@GeneratedValue
	
private int sId;
	//@Column(name="studentName")
private String name;
private String gender;
private String emailId;
private String password;
@ManyToOne
Department department;
public Student() {
	super();
	// TODO Auto-generated constructor stub
}
public Student(int sId, String name, String gender, String emailId, String password, Department department) {
	super();
	this.sId = sId;
	this.name = name;
	this.gender = gender;
	this.emailId = emailId;
	this.password = password;
	this.department = department;
}
public int getsId() {
	return sId;
}
public void setsId(int sId) {
	this.sId = sId;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public Department getDepartment() {
	return department;
}
public void setDepartment(Department department) {
	this.department = department;
}
@Override
public String toString() {
	return "Student [sId=" + sId + ", name=" + name + ", gender=" + gender + ", emailId=" + emailId + ", password="
			+ password + ", department=" + department + "]";
}


}
