package com.ts.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ts.Model.Books;


@Repository
public interface BooksRepository extends JpaRepository<Books,Integer> {

	@Query("from Books where name = :name")
	Books findByName(@Param("name") String name);
}
