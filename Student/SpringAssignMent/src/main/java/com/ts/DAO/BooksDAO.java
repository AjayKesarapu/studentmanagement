package com.ts.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ts.Model.Books;


@Service
public class BooksDAO {
	
	
	@Autowired
	BooksRepository bookrepo;

	public List<Books> getAllBooks() {
		
		return bookrepo.findAll();
	}
	
	
	public  Books getBookbyid(int id){
		
		return bookrepo.findById(id).orElse(null);
	}
	
	public Books getBookByName(String name){
		return  bookrepo.findByName(name);
		
		
	}
	public Books registerBook(Books book){
		return bookrepo.save(book);
		
	}

}
