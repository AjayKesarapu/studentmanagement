package com.ts.DAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ts.Model.BankAccount;
import com.ts.Model.Student;
@Service
public class StudentDAO {
	@Autowired
	StudentRepository studentRepo;
public List<Student> getAllStudents(){
		return studentRepo.findAll();
	}
public Student getStudentbyid(int id){
	
	return studentRepo.findById(id).orElse(null);
}
public Student getStudentByName(String name){
	return  studentRepo.findByName(name);
	}
public Student registerStudent(Student stud){
	return studentRepo.save(stud);
}

public Student updateStu(Student stu) {
	
	return studentRepo.save(stu);
}
public void deleteStuById(int sId) {
	studentRepo.deleteById(sId);
	
	
}
}
